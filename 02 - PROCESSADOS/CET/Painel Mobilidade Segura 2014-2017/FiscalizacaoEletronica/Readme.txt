Data - YYYY-MM-DD HH:MM:SS (Minutos e segundos sempre serão 00)
DiaSemana - dom|seg|ter|qua|qui|sex|sab
CodVeiculo - 6 dígitos (Identificador único referente ao veículo que cometeu a infração)
Enquadramento - Descrição do enquadramento
QtdInfra - Soma das infrações registradas naquele local e horário
Local - Descrição do local onde fiscalização eletrônica estava instalada
Latitude - Latitude
Longitude - Longitude
Origem_LatLong - Manual|Painel MobSegura (Manual caso endereço não constava no mapa exportado pelo site)
