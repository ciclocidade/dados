Arquivos baixados em 29/08/2018, referentes à publicação Julho de 2018. Próxima atualização será em Outubro de 2018.

Para versões atualizadas dos dados do SP156, baixe direto do link: 
http://dados.prefeitura.sp.gov.br/dataset/dados-do-sp156.


Temas relacionados a Mobilidade são:

'Transporte'
'Acessibilidade'
'Trânsito'
'SAC/GRC'


---------------------------------------
Notas de publicação - Julho de 2018
---------------------------------------

Dados do SP156

Os relatórios disponíveis nesta página apresentam informações sobre as solicitações recebidas por meio de todos os canais de atendimento SP156 (Central Telefônica, Portal de Atendimento, Aplicativo Móvel, Praças de Atendimento das Prefeituras Regionais e sistemas integrados ao SP156 de diferentes unidades da Prefeitura, além da Polícia Militar). As bases de dados disponibilizadas estão agregadas por semestre e serão atualizadas trimestralmente pela Secretaria Municipal de Inovação e Tecnologia, tendo recebido tratamento para retirada de informações pessoais e informações sensíveis dos requerentes e de terceiros.

Notas de esclarecimento:

    Os relatórios apresentam informações sobre as solicitações realizadas entre 1º de janeiro de 2012 e 30 de junho de 2018.
    Os relatórios foram extraídos entre os dias 03 e 10 de junho de 2018 e mostram, portanto, como estava o status das solicitações naquelas datas. A próxima atualização dos relatórios está prevista para a primeira quinzena de outubro de 2018.
    Em Novembro de 2016, a Prefeitura implantou um novo sistema de atendimento ao cidadão. Foram migrados para esse sistema somente os dados de 2015 em diante. Assim, os relatórios de 2012 a 2014 e de 2015 a 2018 possuem campos, parâmetros e nomenclaturas de serviços distintos, pois foram extraídos de dois sistemas de atendimento diferentes. Por essa razão, a comparação entre os períodos nem sempre é possível.
    Os dados das publicações recentes podem apresentar pequenos acréscimos da quantidade de protocolos registrados quando comparadas às publicações anteriores. Essa diferença corresponde a protocolos que no momento das primeiras extrações constavam no sistema como rascunho, ou seja, atendimentos não finalizados. Quando novos relatórios são gerados, o sistema contabiliza protocolos que outrora se encontravam na fase de rascunho e que foram posteriormente convertidos em solicitações.
    Todos os trimestres são incluídos novos relatórios e substituídos aqueles publicados anteriormente, pois os status das solicitações podem sofrer alterações, dado o dinamismo da prestação de serviços pelos órgãos municipais. Ou seja, os status dos protocolos podem evoluir de “aguardando atendimento” para “finalizados” ou “indeferidos” em distintos espaços de tempo, de acordo com a natureza da demanda. O prazo para atendimento varia mediante o serviço solicitado, conforme definição de cada órgão responsável pela execução da solução. Estes prazos podem ser conferidos no Guia de Serviços SP156 (https://sp156.prefeitura.sp.gov.br/portal/servicos).
    Destacamos ainda que as solicitações recebidas por meio do SP156 podem ser reclassificadas pelos órgãos prestadores de serviços (por exemplo: uma Prefeitura Regional pode reclassificar uma demanda que foi aberta originalmente como “Tapa-Buraco” para “Tapa-buraco em faixa exclusiva ou corredor de ônibus”). Além disso, as solicitações podem ser encaminhadas de um órgão para outro. Isso torna os relatórios dinâmicos, apresentando diferenças (de nomenclatura de serviço e órgão) entre uma versão e outra, que são consideradas normais.
    Por fim, reafirmamos o nosso comprometimento com a transparência dos dados de atendimento do SP156 e informamos que, gradualmente, a Prefeitura pretende disponibilizar nos relatórios os dados das colunas referentes ao endereço da solicitação. Na última publicação, feita em abril de 2018, os campos de endereço estavam disponíveis para 16 serviços, e agora, na publicação de julho de 2018, 23 serviços já estão com as informações de endereço disponíveis, com cada serviço apresentando diferentes níveis de detalhamento das informações de endereço, de acordo com a particularidade de cada um. Espera-se, nas próximas publicações, aumentar a quantidade de serviços com informações de endereço disponível. De acordo com a Lei Federal nº 12.527/2011 - Lei de Acesso a Informação, esse campo permanece indisponível para os demais serviços listados com vistas a preservar o sigilo de informações pessoais, uma vez que os canais SP156 possuem mais de 300 serviços disponíveis, incluindo serviços de denúncia, casos em que o campo “endereço” é considerado sensível por expor o denunciado ou o denunciante ou possibilitar a identificação de dados de particulares.


