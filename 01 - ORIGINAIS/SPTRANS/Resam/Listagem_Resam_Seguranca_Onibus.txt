Resam
------------------

Art. 5o - Ocorrendo infração será lavrado o "Auto de Infração - AI", conterá, conforme o enquadramento, os itens abaixo:
I - Data da emissão;
II - Número do AI;
III - Código, nome do consórcio ou da empresa operadora ou da cooperativa;
IV - Prefixo do veículo ou placa, quando aplicável;
V - Data e hora da ocorrência;
VI - Sentido (CB, BC, Circular), quando aplicável;
VII - Local (TP, TS, Percurso), quando aplicável;
VIII - Código, tipo e denominação da linha, quando aplicável;
IX - Código alfa numérico correspondente à infração cometida conforme descrição dos Anexos desta Portaria e, quando o caso, breve descrição da infração;
X - Endereço - Local da constatação da infração, quando for o caso;
XI - Valor da multa expresso em reais, quando aplicável;
XII - Prazo para correção, quando aplicável;
XIII - Número do A.I. anterior na hipótese de reincidência;
XIV - Número ou nome do documento de origem (Boletim de Irregularidade, Demonstrativo de Descumprimento de Partida, ou outro);


Listagem dos códigos de infração que podem ser relevantes:
-----------------------------------------------------------------------------------------

PENALIDADES DO PADRÃO DA QUALIDADE

G12 - Entreter-se com jogos em serviço.

PENALIDADES DO PADRÃO DA EFICIÊNCIA

G21 - Deixar de trafegar por corredores exclusivos, faixas de rolamento seletivas ou segregadas não obedecendo à determinação da SPTrans.

PENALIDADES DO PADRÃO DA SEGURANÇA

M41 - Veículo estacionado ou parado sobre a faixa de travessia de pedestres, no Terminal de Transferência

M45 - Operar em velocidade incompatível com a segurança nas proximidades de escolas, hospitais, logradouros estreitos, paradas de embarque e desembarque, estações ou Terminais de Transferência, ou onde haja grande movimentação ou concentração de pessoas, gerando perigo iminente a usuários ou terceiros.

M48 - Motorista fazendo uso em trânsito de sistema de telefonia celular, fone de ouvido, viva-voz ou manter instalado rádio de comunicação (PX, PY), ou qualquer outro sistema de comunicação não autorizado pela SPTrans.

G51 - Veículo estacionado ou parado afastado do meio-fio obrigando os passageiros a embarcarem ou desembarcarem na pista de rolamento.

G56 - Veículo com equipamento registrador instantâneo inalterável de velocidade (tacógrafo) inoperante, com defeito, em mau estado de conservação ou inexistente.

G57 / GR46 - Trafegar acima do limite de velocidade permitido (60Km/h), ou daquele, igual ou inferior estabelecido por sinalização viária do trecho onde tiver lugar a leitura do disco do tacógrafo.

G58 / GR47 - Operar com tacógrafo alterado, não-inspecionado, fora dos padrões de especificação, sem disco de leitura ou com disco de leitura reutilizado, ou com disco de leitura sem registro do prefixo do veículo, ou com disco de leitura sem indicação da data, ou, de qualquer forma, apresentar disco de leitura sem esses registros, com os registros ilegíveis ou adulterados.

G63 / GR48 - Remover, destruir ou, de qualquer forma, impedir o regular funcionamento do dispositivo de monitoramento eletrônico (AVL).

GR26 - Veículo com o sistema de freios com defeito, como: serviço, auxiliar ou de estacionamento

GR30 - Qualquer um dos componentes da tripulação do veículo, funcionário de controle externo da operação, funcionário ligado aos serviços de manutenção, limpeza ou venda de bilhetes ou qualquer outro funcionário ligado à atividade de contato com o público apresentar-se em estado de embriagues alcoólica ou sob efeito de substância tóxica de qualquer natureza.

GR31 - Ingerir bebida alcoólica ou qualquer substância tóxica em serviço.

GR34 - Condutor envolvido em acidente de trânsito, evadir-se do local.

GR37 - Conduzir o veículo de modo a comprometer a segurança dos usuários ou de terceiros.

GR43 - Adotar procedimento irregular na operação colocando os usuários em perigo iminente.

GR45 - Deixar de informar, de imediato, a ocorrência de acidentes com vitimas, envolvendo quaisquer veículos automotivos do sistema de transporte coletivo municipal, ocorrido em via pública ou segregada
